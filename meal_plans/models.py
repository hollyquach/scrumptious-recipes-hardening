from django.db import models


from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.


class MealPlan(models.Model):
    plan_name = models.CharField(max_length=120)
    date_served = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plan",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="mealplan")

    def __str__(self):
        return self.plan_name + " meal plan on " + str(self.date_served)
